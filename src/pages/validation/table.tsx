import React from 'react';
import { observer } from 'mobx-react';

// Component
import { CustomTable } from '../../components/Tables';

// Style
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';

const ValidationTable = observer(({ store }: any) => {
  const columns: Array<Object> = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Local format',
      dataIndex: 'local_format',
      key: 'local_format',
    },
    {
      title: 'Valid',
      dataIndex: 'valid',
      key: 'valid',
      render: (text: boolean) =>
        text ? (
          <CheckOutlined style={{ color: 'green' }} />
        ) : (
          <CloseOutlined style={{ color: 'red' }} />
        ),
    },
    {
      title: 'Carrier',
      dataIndex: 'carrier',
      key: 'carrier',
    },
    {
      title: 'Line type',
      dataIndex: 'line_type',
      key: 'line_type',
    },
    {
      title: 'Country prefix',
      dataIndex: 'country_prefix',
      key: 'country_prefix',
    },
    {
      title: 'Country code',
      dataIndex: 'country_code',
      key: 'country_code',
    },
    {
      title: 'Country name',
      dataIndex: 'country_name',
      key: 'country_name',
    },
  ];

  const data = store.toJS();

  return (
    <div style={{ margin: '50px 0' }}>
      <CustomTable columns={columns} rowKey='id' data={data} />
      Valid phone numbers: {store.validPhoneCount}
    </div>
  );
});

export default ValidationTable;
