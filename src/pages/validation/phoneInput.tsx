import { Button, Input, Form, Spin, message, Select } from 'antd';
import React, { useState } from 'react';
import { PhoneOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react';
import * as api from '../../api';

// Components
const { Option } = Select;

const PhoneInput = observer(({ store }: any) => {
  // use form hook
  const [form] = Form.useForm();
  // use state hook
  const [loading, setLoading] = useState(false);

  const selectedPhone = store.selectedPhone;
  if (selectedPhone) {
    let item = JSON.parse(selectedPhone);
    let prefix = item.country_prefix.replace('+', '');
    let phone = item.local_format;
    form.setFieldsValue({ prefix, phone });
  }

  const prefixSelector = (
    <Form.Item name='prefix' noStyle>
      <Select style={{ width: 180 }}>
        <Option value='1'>United States (+1)</Option>
        <Option value='86'>China (+86)</Option>
        <Option value='852' selected>
          Hong Kong (+852)
        </Option>
      </Select>
    </Form.Item>
  );

  const addValidation = async () => {
    let prefix = form.getFieldValue('prefix');
    let _phone = form.getFieldValue('phone');
    const phone = prefix + _phone;
    if (isNaN(phone)) {
      form.resetFields();
      return message.error('Please enter digits only');
    }
    const isDup = store.checkPhoneExists(phone);
    if (isDup) {
      return message.error('Duplicate phone number!');
    }
    setLoading(true);
    // axios
    let result = await api.phoneApiVerify(phone);
    store.addPhone(phone, result);
    form.resetFields();
    setLoading(false);
  };

  return (
    <div>
      <Spin spinning={loading}>
        <Form
          form={form}
          initialValues={{ prefix: '852' }}
          layout='inline'
          name='control-ref'
        >
          <Form.Item
            name='phone'
            rules={[{ required: true, message: 'Please input phone!' }]}
          >
            <Input
              addonBefore={prefixSelector}
              placeholder='phone'
              size='large'
              prefix={<PhoneOutlined />}
            />
          </Form.Item>

          <Form.Item>
            <Button
              type='primary'
              style={{ marginLeft: 8 }}
              onClick={addValidation}
              size='large'
            >
              Validate
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </div>
  );
});

export default PhoneInput;
