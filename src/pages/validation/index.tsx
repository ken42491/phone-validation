import React from 'react';
import { observer } from 'mobx-react';

// Components
import PhoneInput from './phoneInput';
import PhoneSelector from './phoneSelector';
import ValidationTable from './table';

const ValidationPage = observer(({ store }: any) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      {/* Components */}
      <PhoneSelector store={store} />
      <PhoneInput store={store} />
      <ValidationTable store={store} />
    </div>
  );
});

export default ValidationPage;
