import { Form, Select } from 'antd';
import React from 'react';
import { observer } from 'mobx-react';

// Components
const { Option, OptGroup } = Select;

// Testing Data

const PhoneSelector = observer(({ store }: any) => {
  const data = store.toJS();

  const onSelectValidatedNumber = (phone: string) => {
    store.setPhone(phone);
  };

  return (
    <Form>
      <Form.Item style={{ width: 400 }}>
        <Select
          size='large'
          onChange={onSelectValidatedNumber}
          disabled={data.length === 0}
          placeholder={
            data.length > 0
              ? 'Select your previous validated phone number'
              : 'Please validate at least one phone number first'
          }
        >
          <OptGroup label='Validated Phone Number'>
            {data &&
              data.map((item: any, index: number) => {
                return (
                  <Option value={JSON.stringify(item)} key={index}>
                    {item.phone}
                  </Option>
                );
              }, [])}
          </OptGroup>
        </Select>
      </Form.Item>
    </Form>
  );
});

export default PhoneSelector;
