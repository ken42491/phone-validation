import axios from 'axios';
require('dotenv').config();

const PHONE_API_KEY =
  process.env.PHONE_API_KEY || '7c52cec286e6f35c6345edef68ae488f';

const phoneApiVerify = (phoneNumber: string) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `http://apilayer.net/api/validate?access_key=${PHONE_API_KEY}&format=1&number=${phoneNumber}`
      )
      .then((response) => {
        const result = response.data;
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export { phoneApiVerify };
