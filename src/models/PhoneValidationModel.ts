import {observable} from 'mobx';

export default class PhoneValidationModel {
    store: any;
    id: any;
    @observable phone: string;
    @observable result: object;

    constructor(store: any, id: any, phone: string, result: object) {
        this.store = store;
        this.id = id;
        this.phone = phone;
        this.result = result;
    }

    toJS() {
        return {
            id: this.id,
            phone: this.phone,
            ...this.result
        };
    }
}