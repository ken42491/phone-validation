import React from 'react';
import './App.css';
// import PhoneInput from './components/PhoneInput';
// import ValidationTable from './components/ValidationTable';
import PhoneStore from './stores/PhoneStore';
import ValidationPage from './pages/validation';

import { Layout } from 'antd';
const { Header, Content, Footer } = Layout;

function App() {
  const store = new PhoneStore();

  return (
    <Layout className='layout'>
      <Header>
        <div
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 400,
            textTransform: 'uppercase',
            letterSpacing: 1.5,
          }}
        >
          Serai Phone Validation
        </div>
      </Header>
      <Content
        style={{ padding: '50px 50px', margin: '0 auto', minWidth: 1000 }}
      >
        <div className='App'>
          <ValidationPage store={store} />
        </div>
      </Content>
      <Footer />
    </Layout>
  );
}

export default App;
